# Container and utilities for compression of sequencing files

This repo contains container definition files with tools and utils for compressing sequencing files.
The tools include:

- **orad** for decompression of Dragen-compressed ORA FASTQ files
- **geno(un)zip** for (de)compression of FASTQ, BAM, VCF
- **samtools** for compression of BAM into CRAM
- **crumble** for compression of base quality scores in BAM/CRAM files
