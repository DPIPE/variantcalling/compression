FROM debian:12

LABEL maintainer="OUS AMG <diag-bioinf@medisin.uio.no>"
ENV LC_ALL=C.UTF-8 LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /app

# Install dependencies, genozip, orad, crumble
RUN apt-get update && apt-get install -y curl procps wget libhts3 samtools bcftools bedtools \
build-essential zlib1g-dev libbz2-dev liblzma-dev libcurl4-gnutls-dev libssl-dev libhts-dev

ARG GENOZIP_VERSION=15.0.68
RUN wget https://github.com/divonlan/genozip/archive/refs/tags/genozip-${GENOZIP_VERSION}.tar.gz -nv -O- | tar xvz && \
    cd genozip-genozip-${GENOZIP_VERSION}/installers && \
    tar -xvf genozip-linux-x86_64.tar && \
    mv genozip-linux-x86_64/gen* /usr/local/bin/ && \
    rm -rf genozip-linux-x86_64

RUN wget https://webdata.illumina.com/downloads/software/dragen-decompression/orad.2.6.1.tar.gz && \
    tar -xvzf orad.2.6.1.tar.gz && \
    mv orad_2_6_1/orad /usr/local/bin/ && \
    mv orad_2_6_1/oradata /app/ && \
    rm -rf orad.2.6.1.tar.gz orad_2_6_1

RUN wget https://github.com/jkbonfield/crumble/releases/download/v0.9.1/crumble-0.9.1.tar.gz && \
    tar -xvzf crumble-0.9.1.tar.gz && \
    cd crumble-0.9.1 && \
    ./configure && \
    make && \
    mv crumble /usr/local/bin/ && \
    cd .. && \
    rm -rf crumble-0.9.1.tar.gz crumble-0.9.1

# Mount in the license file from the host when running the container and then run e.g. genozip --licfile ${GENOZIP_LICENSE} mydata.bam
ENV GENOZIP_LICENSE=/app/.genozip_license
ENV ORA_REF_PATH=/app/oradata/

COPY . /app/

CMD ["/bin/bash"]
