DOCKER_BUILDKIT=1
IMAGE_NAME ?= compression:latest
CONTAINER_NAME ?= compression
DATA_DIR ?= $(CURDIR)/data

USER_ID ?= $(shell id -u)
USERGROUP_ID ?= $(shell id -g)

.PHONY: build-image build-singularity-image shell

build-image:
	DOCKER_BUILDKIT=1 docker build -t $(IMAGE_NAME) .

save-image:
	docker save $(IMAGE_NAME) | gzip > $(CONTAINER_NAME).tar.gz

build-singularity-image: build-image
	singularity build compression.sif docker-daemon://$(IMAGE_NAME)

shell:
	docker run --rm -it \
	--name $(CONTAINER_NAME) \
	--mount type=bind,source=$(DATA_DIR),target=/data \
	$(IMAGE_NAME)
